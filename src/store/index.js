import Vue from "vue";
import Vuex from "vuex";
import Auth from "./modules/Auth";
import Societe from "./modules/Societe";
import compteur from "./modules/Compteur";
import historique from "./modules/historique";
import Calcul from "./modules/Calcul";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {},
  getters: {},
  mutations: {},
  actions: {},
  modules: {
    Auth,
    Societe,
    compteur,
    historique,
    Calcul,
  },
});
