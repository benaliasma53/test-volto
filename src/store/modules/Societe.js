import axios from "axios";
import Societe from "../../model/societe";
const state = {
  societes: [],
  errorSocietes: [],
};
const getters = {
  societes: (state) => state.societes,
  errorSocietes: (state) => state.errorSocietes,
};
const mutations = {
  SET_ALL_SOCIETES(state, payload) {
    state.societes = payload.map((item) => Societe.create(item));
  },
  SET_ERROR_SOCIETES(state, payload) {
    state.errorSocietes = payload;
  },
};
const actions = {
  async getSocieteByCompte({ commit }) {
    let data = [
      { id: 1, siret: "siret", RaisonSocial: "RaisonSocial" },
      { id: 2, siret: "siret2", RaisonSocial: "RaisonSocial2" },
    ];
    commit("SET_ALL_SOCIETES", data);
    try {
      const formData = new FormData();
      formData.append("token", localStorage.getItem("token"));
      const response = await axios.post(
        "https://testbackend.smart-electricite.com/api/getSocieteByCompte/",
        formData
      );
      if (response.data.error == false) {
        commit("SET_ALL_SOCIETES", response.data.data);
      } else {
        commit("SET_ERROR_SOCIETES", response.data.type);
      }
      return true;
    } catch (error) {
      return false;
    }
  },
  async ajouterSociete({ commit }, payload) {
    commit("SET_ERROR_SOCIETES", null);
    try {
      const formData = new FormData();
      formData.append("token", localStorage.getItem("token"));
      formData.append("siret", payload?.siret);
      formData.append("RaisonSocial", payload?.RaisonSocial);
      const response = await axios.post(
        "https://testbackend.smart-electricite.com/api/ajouterSociete/",
        formData
      );
      if (response.data.error == false) {
        return true;
      } else {
        commit("SET_ERROR_SOCIETES", response.data.type);
        return false;
      }
    } catch (error) {
      return false;
    }
  },
  async getSocieteBySiret({ commit }, payload) {
    commit("SET_ERROR_SOCIETES", null);
    try {
      const formData = new FormData();
      formData.append("token", localStorage.getItem("token"));
      formData.append("siret", payload);
      const response = await axios.post(
        "https://testbackend.smart-electricite.com/api/getSocieteBySiret/",
        formData,

        {
          headers: {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods":
              "GET, POST, PATCH, PUT, DELETE, OPTIONS",
          },
        }
      );
      if (response.data.error == false) {
        commit("SET_ALL_SOCIETES", [response.data.data]);
      } else {
        commit("SET_ERROR_SOCIETES", response.data.type);
      }
    } catch (error) {
      return false;
    }
  },
  resetError({ commit }) {
    commit("SET_ERROR_SOCIETES", null);
  },
};

export default {
  state,
  getters,
  mutations,
  actions,
};
