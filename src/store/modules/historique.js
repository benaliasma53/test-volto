import axios from "axios";
const state = {
  historiqueS: [
    {
      siret: "historique 1",
      dateDebut: "12/02/2023",
      dateFin: "12/03/2023",
      numCompteur: "numCompteur",
      Montant: 123,
    },
  ],
  errorHistoriqueS: [],
};
const getters = {
  historiqueS: (state) => state.historiqueS,
  errorHistoriqueS: (state) => state.errorHistoriqueS,
};
const mutations = {
  SET_ALL_HISTORIQUE_S(state, payload) {
    state.historiqueS = payload;
  },
  SET_ERROR_HISTORIQUE_S(state, payload) {
    state.errorHistoriqueS = payload;
  },
};
const actions = {
  async getHistoriqueDeCalculeParSociete({ commit }, payload) {
    try {
      const formData = new FormData();
      formData.append("token", localStorage.getItem("token"));
      formData.append("siret", payload);
      const response = await axios.post(
        "https://testbackend.smart-electricite.com/api/getHistoriqueDeCalculeParSociete/",
        formData
      );
      if (response.data.error == false) {
        commit("SET_ALL_HISTORIQUE_S", response.data.data);
      } else {
        commit("SET_ERROR_HISTORIQUE_S", response.data.type);
      }
      return true;
    } catch (error) {
      return false;
    }
  },
  resetErrorHistoriqueS({ commit }) {
    commit("SET_ERROR_HISTORIQUE_S", null);
  },
};

export default {
  state,
  getters,
  mutations,
  actions,
};
