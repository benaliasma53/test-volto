import axios from "axios";
import moment from "moment";
const state = {
  historiqueCompte: [
    {
      siret: "historique 1",
      dateDebut: "12/02/2023",
      dateFin: "12/03/2023",
      numCompteur: "numCompteur",
      Montant: 123,
    },
  ],
  errorHistoriqueCompte: [],
};
const getters = {
  historiqueCompte: (state) => state.historiqueCompte,
  errorHistoriqueCompte: (state) => state.errorHistoriqueCompte,
};
const mutations = {
  SET_HISTORIQUES_COMPTE(state, payload) {
    state.historiqueCompte = payload;
  },
  SET_ERROR_HISTORIQUE_COMPTE(state, payload) {
    state.errorHistoriqueCompte = payload;
  },
};
const actions = {
  async getHistoriqueDeCalculeParCompte({ commit }, payload) {
    try {
      const formData = new FormData();
      formData.append("token", localStorage.getItem("token"));
      formData.append("siret", payload);
      const response = await axios.post(
        "https://testbackend.smart-electricite.com/api/getHistoriqueDeCalculeParCompte/",
        formData
      );
      if (response.data.error == false) {
        commit("SET_HISTORIQUES_COMPTE", response.data.data);
      } else {
        commit("SET_ERROR_HISTORIQUE_COMPTE", response.data.type);
      }
      return true;
    } catch (error) {
      return false;
    }
  },
  async effectuerUnCalcule({ commit }, payload) {
    console.log("test", payload);
    commit("SET_ERROR_HISTORIQUE_COMPTE", null);
    try {
      const formData = new FormData();
      formData.append("token", localStorage.getItem("token"));
      formData.append("siret", payload?.siret);
      formData.append("numCompteur", payload?.numCompteur);
      formData.append(
        "dateDebut",
        moment(payload?.dateDebut).format("DD/MM/YYYY")
      );
      formData.append("dateFin", moment(payload?.dateFin).format("DD/MM/YYYY"));
      const response = await axios.post(
        "https://testbackend.smart-electricite.com/api/effectuerUnCalcule/",
        formData
      );
      if (response.data.error == false) {
        return true;
      } else {
        commit("SET_ERROR_HISTORIQUE_COMPTE", response.data.type);
        return false;
      }
    } catch (error) {
      return false;
    }
  },
  resetErrorHistoriqueCompte({ commit }) {
    commit("SET_ERROR_HISTORIQUE_COMPTE", null);
  },
};

export default {
  state,
  getters,
  mutations,
  actions,
};
