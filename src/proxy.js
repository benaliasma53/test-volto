const http = require('http');
const httpProxy = require('http-proxy');

const proxy = httpProxy.createProxyServer();

http.createServer((req, res) => {
  proxy.web(req, res, { target: 'https://testbackend.smart-electricite.com' });
}).listen(3000);
